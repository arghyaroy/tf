git branch -a > branch.txt
sed -i 1,2d branch.txt  # delete 1st two lines
sed 's/\// /g' branch.txt > branch2.txt  # replace / with space
awk '{print $3}' branch2.txt > branch3.txt # pick 3rd word
sed -r 's/\s+//g' branch3.txt > branch4.txt  # delete extra space

for item in `cat branch4.txt`
do
branch_name=`echo "${item}" | sed 's/ *$//g'`
echo
git checkout ${item}
rm -rf Software_Provisioning.zip
git add .
git commit -m "deleting large file"
git push -u origin ${item}
# if [ $? -eq 0 ];
# then
#        echo "testfile1 is deleted from ${item} branch"
# else
#         echo "testfile1 is not present in ${item} branch"
# fi
# echo 
done


